﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController : ControllerBase
    {

        private readonly IRepository<PromoCode> _promoCodeRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<Customer> _customerRepository;

        public PromocodesController(IRepository<PromoCode> promoCodeRepository,
            IRepository<Preference> preferenceRepository, IRepository<Customer> customerRepository)
        {
            _promoCodeRepository = promoCodeRepository;
            _preferenceRepository = preferenceRepository;
            _customerRepository = customerRepository;
        }

        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var customers = await _promoCodeRepository.GetAllAsync();

            var response = customers.Select(x => new PromoCodeShortResponse()
            {
                Id = x.Id,
                Code = x.Code,
                ServiceInfo = x.ServiceInfo,
                BeginDate = x.BeginDate.ToString(),
                EndDate = x.EndDate.ToString(),
                PartnerName = x.PartnerName
            }).ToList();

            return Ok(response);
        }
        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var preference = await _preferenceRepository.GetPreferenceByNameAsync(request.Preference);

            var promoCode = new PromoCode()
            {
                Id = Guid.NewGuid(),
                ServiceInfo = request.ServiceInfo,
                PartnerName = request.PartnerName,
                Preference = preference,
                Code = request.PromoCode
            };
            await _promoCodeRepository.CreateAsync(promoCode);

            if (preference == null) return Ok();

            var allCustomers = await _customerRepository.GetCustomerByPreferenceAsync(preference);

            foreach (var customer in allCustomers)
            {
                customer.PromoCodes.Add(promoCode);
                await _customerRepository.UpdateAsync(customer);
            }

            return Ok();
        }
    }
}