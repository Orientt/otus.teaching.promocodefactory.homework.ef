﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Customer :BaseEntity
    {
        [MaxLength(50, ErrorMessage = "Length must be less then 50 characters")]
        public string FirstName { get; set; }
        [MaxLength(50, ErrorMessage = "Length must be less then 50 characters")]
        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        [MaxLength(50, ErrorMessage = "Length must be less then 50 characters")]
        public string Email { get; set; }

        //TODO: Списки Preferences и Promocodes 

        public virtual ICollection<CustomerPreference> Preferences { get; set; }

        public virtual ICollection<PromoCode> PromoCodes { get; set; }

        public string PropertyForMigration { get; set; }
    }
}