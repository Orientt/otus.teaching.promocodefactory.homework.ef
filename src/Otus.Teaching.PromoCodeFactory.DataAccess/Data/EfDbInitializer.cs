﻿
using Microsoft.EntityFrameworkCore;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class EfDbInitializer : IDbInitializer

    {
        public readonly DataContext _dataContext;

        public EfDbInitializer(DataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public void InitializeDb()
        {
            _dataContext.Database.Migrate();

            _dataContext.AddRange(FakeDataFactory.Employees);
            _dataContext.SaveChangesAsync();

            _dataContext.AddRange(FakeDataFactory.Preferences);
            _dataContext.SaveChangesAsync();

            _dataContext.AddRange(FakeDataFactory.Customers);
            _dataContext.SaveChangesAsync();
        }
    }
}
